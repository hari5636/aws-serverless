package domain
import javaposse.jobdsl.dsl.Job

class EnvironmentSetup{
  static void applyTo(Job job, Map config) {
    job.with {
      parameters {
        labelParam("NODE_LABEL") {
          defaultValue(config.label ?: "master || release-slave")
        }
        labelParam("BRANCH") {
          defaultValue("master")
        }
        labelParam("APP_CODE") {
        }
        labelParam("STAGE") {
        }
        labelParam("READONLY_ACCESS_GROUP") {
        }
        labelParam("FULL_ACCESS_GROUP") {
        }
      }
      wrappers{
        preBuildCleanup()
      }
      authorization {
        permissions('authenticated', [
                'hudson.model.Item.Build',
                'hudson.model.Item.Cancel',
        ])
      }
      definition{
        cps {
          script(config.script ?: "")
          sandbox()
        }
      }
    }
  }  
}

