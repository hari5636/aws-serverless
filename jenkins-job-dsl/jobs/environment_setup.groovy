import domain.EnvironmentSetup

def environmentSetup = {Map config ->
  EnvironmentSetup.applyTo(pipelineJob("environment-setup"), [
    script : readFileFromWorkspace('jenkins-job-dsl/jobs/SetupPipeline.groovy')
  ])
}

environmentSetup()
