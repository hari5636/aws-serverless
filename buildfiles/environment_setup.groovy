import domain.EnvironmentSetup

def environmentSetup = {Map config ->
  EnvironmentSetup.applyTo(pipelineJob("environment-setup"), [
    script : readFileFromWorkspace('SetupPipeline.groovy')
  ])
}

environmentSetup()
